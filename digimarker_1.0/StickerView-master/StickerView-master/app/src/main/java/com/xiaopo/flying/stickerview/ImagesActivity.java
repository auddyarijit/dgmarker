package com.xiaopo.flying.stickerview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.Layout;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.util.LruCache;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;

import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.getbase.floatingactionbutton.FloatingActionsMenu;


import com.google.android.material.floatingactionbutton.FloatingActionButton;

import com.xiaopo.flying.sticker.DrawableSticker;
import com.xiaopo.flying.sticker.StickerView;
import com.xiaopo.flying.sticker.TextSticker;
import com.xiaopo.flying.stickerview.adapter.MyListAdapter;
import com.xiaopo.flying.stickerview.adapter.NumberAdapter;
import com.xiaopo.flying.stickerview.adapter.PageNoAdapter;
import com.xiaopo.flying.stickerview.adapter.QuestionMarksAdapter;
import com.xiaopo.flying.stickerview.adapter.ThumbnailAdapter;
import com.xiaopo.flying.stickerview.adapter.ViewImageAdapter;
import com.xiaopo.flying.stickerview.model.BookletResponse;
import com.xiaopo.flying.stickerview.pdf.utils.PDFCreationUtils;
import com.xiaopo.flying.stickerview.pdf.utils.PdfBitmapCache;
import com.xiaopo.flying.stickerview.util.FileUtil;
import com.xiaopo.flying.stickerview.util.uuuuuu.BubbleInputDialog;
import com.xiaopo.flying.stickerview.util.uuuuuu.BubbleTextView;
import com.xiaopo.flying.stickerview.util.uuuuuu.DemoStickerView;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.xiaopo.flying.stickerview.adapter.ThumbnailAdapter.check;
import static com.xiaopo.flying.stickerview.adapter.ThumbnailAdapter.globalPosition;

public class ImagesActivity extends AppCompatActivity implements View.OnClickListener {

    private static int queNo;
    private static int posss;
    private static int number;

    private String directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/myCamera/";
    private static int cordX, cordY;

    ThumbnailAdapter.InvoiceHistoryViewHolder holder;

    int pagePostion;

    ThumbnailAdapter adapter;
    static ViewImageAdapter viewImageAdapter;
    QuestionMarksAdapter queAdapter;
    PageNoAdapter pageNoAdapter;
    NumberAdapter numberAdapter;


    MyListAdapter adapter1;
    RecyclerView recyclerView;
    Button btnAdd;
    Button btnQue;

    String commentext;
    static Bitmap bitmapBooklet;


    int i = 0;
    int totalSum = 0;

    static int bitmapPos = 0;


    //for pdf creation
    private boolean IS_MANY_PDF_FILE;

    /**
     * This is identify to number of pdf file. If pdf model list size > sector so we have create many file. After that we have merge all pdf file into one pdf file
     */
    private int SECTOR = 1; // Default value for one pdf file.
    private int START;
    private int END = SECTOR;
    private int NO_OF_PDF_FILE = 1;
    private int NO_OF_FILE;
    private int LIST_SIZE;
    private ProgressDialog progressDialog;


//    public void check() {
//    }

    // public StickerView stickerView;
    private TextSticker sticker;
    private ImageView imgCircle;
    private ImageView imgCross;
    private ImageView imgTick;
    private ImageView imgQuestion;
    private ImageView imgText;
    private ImageView imgSquare;
    private ImageView imgRemark;
    private RecyclerView rvQuestion;

    private TextView txtTotal;
    private LinearLayout llThumbnailView;
    private LinearLayout llModelAnswer;



    static int globalImage = R.drawable.remark_text;

    Bitmap boloBitmap;
    Bitmap remarkBitmap;


    public static int getGlobalImage() {
        return globalImage;
    }


    private RelativeLayout mContentRootView;

    int x, y;
    // List<PhotoResponse> photoResponse = new ArrayList<>();


    ArrayList<Integer> queList = new ArrayList<Integer>();
    ArrayList<Integer> marksList = new ArrayList<Integer>();
    ArrayList<Integer> totalMarksList = new ArrayList<Integer>();
    ArrayList<Integer> pageNoList = new ArrayList<Integer>();
    public static List<StickerView> ivProfilePicArr = new ArrayList<StickerView>();

    public static ArrayList<Bitmap> bitmapArrayList = new ArrayList<>();


    private List<BookletResponse> bookletResponseList = new ArrayList<>();


    private ArrayList<View> mViews;
    DemoStickerView mCurrentView;
    private BubbleTextView mCurrentEditTextView;
    private BubbleInputDialog mBubbleInputDialog;

    private FloatingActionsMenu mMultipleActions;
    private View btnFltSample;
    private View btnFltBookletView;
    RelativeLayout rl_content_root;

    public StickerView sticker_view;

    String str;
    String date;
    String time;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        //stickerView = (StickerView) findViewById(R.id.sticker_view);
        imgCircle = (ImageView) findViewById(R.id.imgCircle);
        imgCross = (ImageView) findViewById(R.id.imgCross);
        imgTick = (ImageView) findViewById(R.id.imgTick);
        imgQuestion = (ImageView) findViewById(R.id.imgQuestion);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnQue = (Button) findViewById(R.id.btnQue);
        imgSquare = (ImageView) findViewById(R.id.imgSquare);
        imgRemark = (ImageView) findViewById(R.id.imgRemark);
        rvQuestion = findViewById(R.id.rvQuestion);
        sticker_view = findViewById(R.id.sticker_view);

        txtTotal = findViewById(R.id.txtTotal);
        llThumbnailView = findViewById(R.id.llThumbnailView);
        llModelAnswer = findViewById(R.id.llModelAnswer);


        mMultipleActions = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        btnFltSample = findViewById(R.id.btnFltSample);
        btnFltBookletView = findViewById(R.id.btnFltBookletView);
        rl_content_root = findViewById(R.id.rl_content_root);


        //  stickerView.addSticker(imgBubble);
        imgCircle.setOnClickListener(this);
        imgCross.setOnClickListener(this);
        imgTick.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        imgQuestion.setOnClickListener(this);
        imgSquare.setOnClickListener(this);
        btnQue.setOnClickListener(this);
        llThumbnailView.setOnClickListener(this);
        llModelAnswer.setOnClickListener(this);

        btnFltBookletView.setOnClickListener(this);
        imgRemark.setOnClickListener(this);

        sticker = new TextSticker(this);

        queList.add(1);
        queList.add(2);
        queList.add(3);
        queList.add(4);
        queList.add(5);
        queList.add(6);
        queList.add(7);
        queList.add(8);
        queList.add(9);
        queList.add(10);


        marksList.add(1);
        marksList.add(2);
        marksList.add(3);
        marksList.add(4);
        marksList.add(5);


        pageNoList.add(1);
        pageNoList.add(2);
        pageNoList.add(3);
        pageNoList.add(4);
        pageNoList.add(5);


        btnFltSample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // addBubble();
                Intent intent = new Intent(ImagesActivity.this, SampleQuestionActivity.class);
                startActivity(intent);
                mMultipleActions.collapse();
            }
        });


        mViews = new ArrayList<>();
        mBubbleInputDialog = new BubbleInputDialog(this);
        mBubbleInputDialog.setCompleteCallBack(new BubbleInputDialog.CompleteCallBack() {
            @Override
            public void onComplete(View bubbleTextView, String str) {
                ((BubbleTextView) bubbleTextView).setText(str);
            }
        });


        setAdapterBooklet();

        //permission();
        addList();

        currentDate();
        currentTime();

        drawText(date + " " + time, 35);

    }


    private void currentDate() {
        date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Log.d("currentDate", "currentDate: " + date);
    }

    private void currentTime() {
        time = new SimpleDateFormat("HH:mm:SS").format(new Date());
        Log.d("time", "time: " + time);
    }

    private void addList() {
        BookletResponse bookletResponse = new BookletResponse();
        bookletResponse.setImage(R.drawable.image1);
        bookletResponseList.add(bookletResponse);

        bookletResponse = new BookletResponse();
        bookletResponse.setImage(R.drawable.image2);
        bookletResponseList.add(bookletResponse);

        bookletResponse = new BookletResponse();
        bookletResponse.setImage(R.drawable.image3);
        bookletResponseList.add(bookletResponse);

        bookletResponse = new BookletResponse();
        bookletResponse.setImage(R.drawable.image1);
        bookletResponseList.add(bookletResponse);

        bookletResponse = new BookletResponse();
        bookletResponse.setImage(R.drawable.image2);
        bookletResponseList.add(bookletResponse);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 111 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //generatePdfReport();
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 111);
        } else {
            //  generatePdfReport();
        }
    }



    public static Bitmap getBitmapFromView(View view) {

        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null) {
            bgDrawable.draw(canvas);
        } else {
            canvas.drawColor(Color.TRANSPARENT);
        }
        view.draw(canvas);
        return returnedBitmap;

    }


    @SuppressLint("StringFormatInvalid")
    private void createProgressBarForPDFCreation(int maxProgress) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(String.format(getString(R.string.msg_progress_pdf), String.valueOf(maxProgress)));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMax(maxProgress);
        progressDialog.show();
    }

    private void createProgressBarForMergePDF() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.msg_progress_merger_pdf));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    public void addBubble(ThumbnailAdapter.InvoiceHistoryViewHolder position) {
        final BubbleTextView bubbleTextView = new BubbleTextView(this,
                Color.RED, 0);
        bubbleTextView.setImageResource(R.drawable.bubble);
        bubbleTextView.setMaxHeight(40);
        bubbleTextView.setMinimumWidth(30);
        bubbleTextView.setOperationListener(new BubbleTextView.OperationListener() {
            @Override
            public void onDeleteClick() {
                mViews.remove(bubbleTextView);
                //  rl_content_root.removeView(bubbleTextView);
                sticker_view.removeView(bubbleTextView);
            }

            @Override
            public void onEdit(BubbleTextView bubbleTextView) {
                if (mCurrentView != null) {
                    mCurrentView.setInEdit(false);
                }
                mCurrentEditTextView.setInEdit(false);
                mCurrentEditTextView = bubbleTextView;
                mCurrentEditTextView.setInEdit(true);
            }

            @Override
            public void onClick(BubbleTextView bubbleTextView) {
                mBubbleInputDialog.setBubbleTextView(bubbleTextView);
                mBubbleInputDialog.show();
            }

            @Override
            public void onTop(BubbleTextView bubbleTextView) {
                int position = mViews.indexOf(bubbleTextView);
                if (position == mViews.size() - 1) {
                    return;
                }
                BubbleTextView textView = (BubbleTextView) mViews.remove(position);
                mViews.add(mViews.size(), textView);
            }
        });
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        // rl_content_root.addView(bubbleTextView, lp);
        sticker_view.addView(bubbleTextView, lp);
        mViews.add(bubbleTextView);
        setCurrentEdit(bubbleTextView);
    }


    private void setCurrentEdit(BubbleTextView bubbleTextView) {
        if (mCurrentView != null) {
            mCurrentView.setInEdit(false);
        }
        if (mCurrentEditTextView != null) {
            mCurrentEditTextView.setInEdit(false);
        }
        mCurrentEditTextView = bubbleTextView;
        mCurrentEditTextView.setInEdit(true);
    }


    private void setAdapterBooklet() {

//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(mLayoutManager);
//        ThumbnailAdapter pdfRootAdapter = new ThumbnailAdapter();
//        pdfRootAdapter.setListData(bookletResponseList);
//        recyclerView.setAdapter(pdfRootAdapter);

        adapter = new ThumbnailAdapter(this, bookletResponseList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter.setListData(bookletResponseList);
        recyclerView.setAdapter(adapter);
    }


    private void setAdapterQuestion() {
        queAdapter = new QuestionMarksAdapter(this, queList);
        rvQuestion.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvQuestion.setLayoutManager(layoutManager);
        rvQuestion.setAdapter(queAdapter);
    }


    private void setAdapterPageNo() {
        pageNoAdapter = new PageNoAdapter(this, pageNoList);
        rvQuestion.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvQuestion.setLayoutManager(layoutManager);
        rvQuestion.setAdapter(pageNoAdapter);
    }


    public void setAdapterNumber() {
        numberAdapter = new NumberAdapter(this, marksList);
        rvQuestion.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvQuestion.setLayoutManager(layoutManager);
        rvQuestion.setAdapter(numberAdapter);
    }


    public int getQuestionNumberPosition(int Quepos) {
        Toast.makeText(this, "" + Quepos, Toast.LENGTH_SHORT).show();
        queNo = Quepos;

        return queNo;
    }


    public void drawText(String text, int textSize) {


        // Get text dimensions
        TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG
                | Paint.LINEAR_TEXT_FLAG);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(textSize);

        StaticLayout mTextLayout = new StaticLayout(text, textPaint,
                200, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

        // Create bitmap and canvas to draw to
        boloBitmap = Bitmap.createBitmap(200, mTextLayout.getHeight(), Bitmap.Config.RGB_565);
        Canvas c = new Canvas(boloBitmap);

        // Draw background
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG
                | Paint.LINEAR_TEXT_FLAG);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setColor(Color.WHITE);
        c.drawPaint(paint);

        // Draw text
        c.save();
        c.translate(0, 0);
        mTextLayout.draw(c);
        c.restore();

        Log.d("TAG", "drawText: " + boloBitmap);
    }


    public int getNumberPosition(int numPostion) {

        Toast.makeText(this, "" + numPostion, Toast.LENGTH_SHORT).show();
        number = numPostion;

        String answer = "a_" + number;

        String question = "q_" + queNo;

        int dddd = 0;
        ;
        String vvvv = "value/A";

        try {
            dddd = Integer.parseInt(vvvv);
        } catch (NumberFormatException e) {
            System.out.println("not a number");
        }


        if (queNo>0) {

            int qqq = getResources().getIdentifier(question, "drawable", getPackageName());
            ivProfilePicArr.add(sticker_view);
            Drawable queDra = ContextCompat.getDrawable(this, qqq);
            StickerView queS = check(pagePostion).addSticker(new DrawableSticker(queDra), 0 | 0);
            queS.getCurrentSticker().getMatrix().setTranslate(cordX - 80, cordY - 20);


            if (vvvv != null) {

                Drawable mDrawable = new BitmapDrawable(getResources(), boloBitmap);
                // dddd = getResources().getIdentifier(variableValue, "drawable", getPackageName());
                ivProfilePicArr.add(sticker_view);
                //  boloBitmap = ContextCompat.getDrawable(this, dddd);
                StickerView sss = check(pagePostion).addSticker(new DrawableSticker(mDrawable), 0 | 0);
                sss.getCurrentSticker().getMatrix().setTranslate(cordX + 100, cordY - 100);
                Log.d("TAG", "yoo: " + dddd);
            }

            if (answer != null) {
                int immm = getResources().getIdentifier(answer, "drawable", getPackageName());
                ivProfilePicArr.add(sticker_view);
                Drawable drawable1 = ContextCompat.getDrawable(this, immm);
                StickerView s = check(pagePostion).addSticker(new DrawableSticker(drawable1), 0 | 0);
                s.getCurrentSticker().getMatrix().setTranslate(cordX + 60, cordY - 20);
            }


            totalMarksList.add(numPostion);

            getTotalMarks(totalMarksList, numPostion);

        } else {
            Toast.makeText(this, "Please select question number", Toast.LENGTH_SHORT).show();
        }

        return numPostion;

    }

    private void getQuestionNumber() {
    }


//    private void permission() {
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED
//                || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this,
//                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 110);
//        } else {
//            //generatePdfReport();
//            // loadSticker();
//        }
//    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode == 110 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            // loadSticker();
//          //  generatePdfReport();
//        }
//    }


    /**
     * This is manage to all model
     */


//    private void loginApi() {
//        Call<List<PhotoResponse>> call = RetrofitClient
//                .getInstance()
//                .getApi()
//                .photoResponse(1);
//        call.enqueue(new Callback<List<PhotoResponse>>() {
//            @Override
//            public void onResponse(Call<List<PhotoResponse>> call, Response<List<PhotoResponse>> response) {
//                photoResponse = response.body();
//                setAdapter();
//                Toast.makeText(ImagesActivity.this, "success", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onFailure(Call<List<PhotoResponse>> call, Throwable t) {
//                Toast.makeText(ImagesActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
//
//            }
//        });
//
//    }
    private void setAdapter() {
//        adapter = new ThumbnailAdapter(this, photoResponse);
//        RecyclerView.LayoutManager histrory = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//        recyclerView.setLayoutManager(histrory);
//        recyclerView.setAdapter(adapter);

    }


    public void testAdd() {
        //ThumbnailAdapter.setText((commentext));
        final TextSticker sticker = new TextSticker(this);
        sticker.setText(commentext);
//        sticker.setTextColor(Color.BLUE);
        sticker.setTextAlign(Layout.Alignment.ALIGN_CENTER);
        sticker.resizeText();
//        stickerView.addSticker(sticker);
        StickerView s = check(ThumbnailAdapter.globalPosition).addSticker(sticker, 0 | 0);
        s.getCurrentSticker().getMatrix().setTranslate(ThumbnailAdapter.xCord, ThumbnailAdapter.yCord);
        //stickerView.getCurrentSticker().getMatrix().setTranslate(ThumbnailAdapter.xCord, ThumbnailAdapter.yCord);
        Log.d("xCord", "xCord: " + ThumbnailAdapter.xCord + ThumbnailAdapter.yCord);
        Log.d("globalPosition", "globalPosition: " + ThumbnailAdapter.globalPosition + " " + ThumbnailAdapter.xCord + " " + ThumbnailAdapter.yCord);
    }


    public static ArrayList<Integer> getValue() {
        int circlePosition = queNo;
        int circleId = R.drawable.circle;
        ArrayList<Integer> ids = new ArrayList<Integer>();
        ids.add(circlePosition);
        ids.add(circleId);
        return ids;
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgCircle:
                this.globalImage = R.drawable.circle;
                int circlePosition = queNo;
                int circleId = R.drawable.circle;
                //setAdapterNumber(queNo);
                //  getValue();
                Toast.makeText(this, "" + circlePosition + " " + circleId, Toast.LENGTH_SHORT).show();
                Log.d("TAG", "position:    circleId  " + circlePosition + " " + circleId);

                break;


            case R.id.imgCross:
                this.globalImage = R.drawable.close;
                break;


            case R.id.imgTick:
                this.globalImage = R.drawable.tick;
                break;


            case R.id.imgQuestion:
                this.globalImage = R.drawable.question_marks;
                break;


            case R.id.imgSquare:
                this.globalImage = R.drawable.square;
                break;


            case R.id.btnQue:
                if (i == 0) {
                    setAdapterQuestion();
                    rvQuestion.setVisibility(View.VISIBLE);
                    i++;
                } else if (i == 1) {
                    rvQuestion.setVisibility(View.GONE);
                    i = 0;
                }
                break;


//            case R.id.float_page:
//                if (i == 0) {
//                    setAdapterPageNo();
//                    rvQuestion.setVisibility(View.VISIBLE);
//                    i++;
//                } else if (i == 1) {
//                    rvQuestion.setVisibility(View.GONE);
//                    i = 0;
//                }
//                break;


            case R.id.btnFltBookletView:
                getBitmapByView(recyclerView);
                viewImageAdapter.notifyDataSetChanged();
                Intent i = new Intent(ImagesActivity.this, BookletViewActivity.class);
                i.putExtra("bitmapPos", bitmapPos);
                startActivity(i);
                break;


            case R.id.imgRemark:
                this.globalImage = R.drawable.remark_text;
                remarkAlert();
                break;


            case R.id.btnAdd:
                requestPermission();
                break;


            case R.id.llModelAnswer:
                Intent intentSample = new Intent(ImagesActivity.this, SampleQuestionActivity.class);
                startActivity(intentSample);
                break;


            case R.id.llThumbnailView:
                getBitmapByView(recyclerView);
                Intent intent = new Intent(ImagesActivity.this, BookletViewActivity.class);
                intent.putExtra("bitmapPos", bitmapPos);
                startActivity(intent);
                break;

        }
    }


    private void remarkAlert() {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(this);
        View mView = layoutInflaterAndroid.inflate(R.layout.remark_layout, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(this);
        alertDialogBuilderUserInput.setView(mView);

        final EditText edtRemark = (EditText) mView.findViewById(R.id.edtRemark);
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        String remark = edtRemark.getText().toString();
                        drawRemark(remark, 30);
                    }
                })

                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.show();

    }


    public void drawRemark(String text, int textSize) {
        TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
        textPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        textPaint.setColor(Color.RED);
        textPaint.setTextSize(textSize);

        StaticLayout mTextLayout = new StaticLayout(text, textPaint,
                200, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

        // Create bitmap and canvas to draw to
        remarkBitmap = Bitmap.createBitmap(200, 40, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(remarkBitmap);

        // Draw background
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG
                | Paint.LINEAR_TEXT_FLAG);

        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setColor(Color.WHITE);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(20);
        c.drawPaint(paint);

        // Draw text
        c.save();
        c.translate(0, 0);
        mTextLayout.draw(c);
        c.restore();


        Drawable mDrawable = new BitmapDrawable(getResources(), remarkBitmap);
        ivProfilePicArr.add(sticker_view);
        StickerView sss = check(pagePostion).addSticker(new DrawableSticker(mDrawable), 0 | 0);
        sss.getCurrentSticker().getMatrix().setTranslate(cordX, cordY);
        Log.d("TAG", "drawText: " + remarkBitmap);
    }


    public static ArrayList<Bitmap> getData() {
        return bitmapArrayList;
    }


    public static Bitmap getBitmapByView(RecyclerView view) {
        bitmapBooklet = null;
        for (int i = 0; i < bitmapPos; i++) {
            bitmapBooklet = Bitmap.createBitmap(
                    view.getChildAt(i).getWidth(),
                    view.getChildAt(i).getHeight(),
                    Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(bitmapBooklet);
            view.getChildAt(i).draw(c);
            bitmapArrayList.add(bitmapBooklet);
        }
        return bitmapBooklet;
    }


    private void save() {
        File file = FileUtil.getNewFile(ImagesActivity.this, "Sticker");
        if (file != null) {
            sticker_view.save(file);
            Toast.makeText(ImagesActivity.this, "saved in " + file.getAbsolutePath(),
                    Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(ImagesActivity.this, "the file is null", Toast.LENGTH_SHORT).show();
        }


    }


    public void text() {
        commentDailog();
    }


    private void commentDailog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Remark");

        // set the custom layout
        final View customLayout = getLayoutInflater().inflate(R.layout.custom_layout, null);
        builder.setView(customLayout);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditText editText = customLayout.findViewById(R.id.editText);
                commentext = sendDialogDataToActivity(editText.getText().toString());
                testAdd();
            }
        });

        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                }
        );

        // create and show
        // the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // Do something with the data
    // coming from the AlertDialog
    private String sendDialogDataToActivity(String data) {
        Toast.makeText(this, data, Toast.LENGTH_SHORT).show();
        return data;
    }


    public int getCordinatesX(int x1) {
        cordX = x1;
        return cordX;
    }

    public int getCordinatesY(int y1) {
        cordY = y1;
        return cordY;
    }

    public int numberPosition(ThumbnailAdapter.InvoiceHistoryViewHolder holder, int numPosition) {

        pagePostion = numPosition;
        return pagePostion;
    }

    public void getTotalMarks(ArrayList<Integer> totalMarksList, int i) {

        if (totalSum < 100) {
            totalSum += i;
           // txtTotal.setText("Total\n" + String.valueOf(totalSum) + "/100");
            txtTotal.setText(totalSum + "/100");

        } else {
            Toast.makeText(this, "Some thing went wrong", Toast.LENGTH_SHORT).show();
        }
    }


    public void getPageNoPosition(PageNoAdapter.PageNoViewholder holder, int pageNumPostion) {

    }


    public Bitmap bitmapPostion(ThumbnailAdapter.InvoiceHistoryViewHolder holder, int globalPosition) {

        Bitmap bitmap = null;

        ArrayList<Bitmap> bitmapArrayList = new ArrayList<>();


        for (int i = 0; i < globalPosition; i++) {
            bitmap = Bitmap.createBitmap(
                    recyclerView.getChildAt(i).getWidth(),
                    recyclerView.getChildAt(i).getHeight(),
                    Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(bitmap);
            recyclerView.getChildAt(i).draw(c);
            bitmapArrayList.add(bitmap);
        }
        return bitmap;
    }

    public int bitmapPostion(int bitmapPosss) {
        bitmapPos = bitmapPosss;
        return bitmapPos;
    }
}
