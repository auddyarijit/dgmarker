package com.xiaopo.flying.stickerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.xiaopo.flying.stickerview.adapter.ViewImageAdapter;
import com.xiaopo.flying.stickerview.pdf.utils.PDFCreationUtils;
import com.xiaopo.flying.stickerview.pdf.utils.PdfBitmapCache;

import java.util.ArrayList;
import java.util.List;

public class BookletViewActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private  ViewImageAdapter viewImageAdapter;
    static ArrayList<Bitmap> bitmapArrayList = new ArrayList<>();
    ImageView imgBack;

    static int bitmapPos =0;

    FloatingActionButton fltBtn;

    private boolean IS_MANY_PDF_FILE;
    private int SECTOR = 1;
    private int START;
    private int END = SECTOR;
    private int NO_OF_PDF_FILE = 1;
    private int NO_OF_FILE;
    private int LIST_SIZE;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booklet_view);

        recyclerView = findViewById(R.id.recyclerView);
        imgBack = findViewById(R.id.imgBack);
        fltBtn = findViewById(R.id.fltBtn);

        imgBack.setOnClickListener(this);
        fltBtn.setOnClickListener(this);

        bitmapArrayList = ImagesActivity.getData();

        getDataa();

        setAdapterBitmap();
       // getBitmapByView(recyclerView);
    }




    private ArrayList<Bitmap> getDataa() {
         Intent intent = getIntent();
         bitmapPos = intent.getIntExtra("bitmapPos", 0);
       //  bitmapArrayList = getIntent().getStringArrayListExtra("bitmapArrayList");
     //    Log.d("bitmapArrayList", "bitmapArrayList: " + bitmap);
        return null;
    }

    private void setAdapterBitmap() {
       // viewImageAdapter = new ViewImageAdapter(this,bitmapArrayList);
//        viewImageAdapter = new ViewImageAdapter(bitmapArrayList);
//        recyclerView.setHasFixedSize(true);
//        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setAdapter(viewImageAdapter);

        viewImageAdapter = new ViewImageAdapter(this,bitmapArrayList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        viewImageAdapter.setListData(bitmapArrayList);
        recyclerView.setAdapter(viewImageAdapter);
        viewImageAdapter.notifyDataSetChanged();
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 111 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            generatePdfReport();
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 111);
        } else {
            generatePdfReport();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.imgBack:
               finish();
                break;


            case R.id.fltBtn:
                requestPermission();
                break;
        }
    }


    private void generatePdfReport() {

        LIST_SIZE = bitmapArrayList.size();
        NO_OF_FILE = LIST_SIZE / SECTOR;
        if (LIST_SIZE % SECTOR != 0) {
            NO_OF_FILE++;
        }
        if (LIST_SIZE > SECTOR) {
            IS_MANY_PDF_FILE = true;
        } else {
            END = LIST_SIZE;
        }
        createPDFFile();
    }

    /**
     * This function call with recursion
     * This recursion depend on number of file (NO_OF_PDF_FILE)
     */
    private void createPDFFile() {
        // Find sub list for per pdf file data
        List<Bitmap> pdfDataList = bitmapArrayList.subList(START, END);
        PdfBitmapCache.clearMemory();
        PdfBitmapCache.initBitmapCache(getApplicationContext());
        final PDFCreationUtils pdfCreationUtils = new PDFCreationUtils(BookletViewActivity.this, pdfDataList, LIST_SIZE, NO_OF_PDF_FILE);
        if (NO_OF_PDF_FILE == 1) {
            createProgressBarForPDFCreation(PDFCreationUtils.TOTAL_PROGRESS_BAR);
        }
        pdfCreationUtils.createPDF(new PDFCreationUtils.PDFCallback() {

            @Override
            public void onProgress(final int i) {
                progressDialog.setProgress(i);
            }

            @Override
            public void onCreateEveryPdfFile() {
                // Execute may pdf files and this is depend on NO_OF_FILE
                if (IS_MANY_PDF_FILE) {
                    NO_OF_PDF_FILE++;
                    if (NO_OF_FILE == NO_OF_PDF_FILE - 1) {

                        progressDialog.dismiss();
                        createProgressBarForMergePDF();
                        pdfCreationUtils.downloadAndCombinePDFs();
                    } else {
                        // This is identify to manage sub list of current pdf model list data with START and END
                        START = END;
                        if (LIST_SIZE % SECTOR != 0) {
                            if (NO_OF_FILE == NO_OF_PDF_FILE) {
                                END = (START - SECTOR) + LIST_SIZE % SECTOR;
                            }
                        }
                        END = SECTOR + END;
                        createPDFFile();
                    }

                } else {
                    // Merge one pdf file when all file is downloaded
                    progressDialog.dismiss();

                    createProgressBarForMergePDF();
                    pdfCreationUtils.downloadAndCombinePDFs();
                }

            }

            @Override
            public void onComplete(final String filePath) {
                progressDialog.dismiss();

                if (filePath != null) {
//                    btnPdfPath.setVisibility(View.VISIBLE);
//                    btnPdfPath.setText("PDF path : " + filePath);
//                    Toast.makeText(PdfCreationActivity.this, "pdf file " + filePath, Toast.LENGTH_LONG).show();
//                    btnSharePdfFile.setVisibility(View.VISIBLE);
//                    btnSharePdfFile.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            sharePdf(filePath);
//                        }
//                    });

                }
            }

            @Override
            public void onError(Exception e) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Toast toast = Toast.makeText(BookletViewActivity.this, "Error"+ e.getMessage(), Toast.LENGTH_SHORT);
                        toast.show();
                    }
                });
                // Toast.makeText(ImagesActivity.this, "Error  " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }



    @SuppressLint("StringFormatInvalid")
    private void createProgressBarForPDFCreation(int maxProgress) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(String.format(getString(R.string.msg_progress_pdf), String.valueOf(maxProgress)));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMax(maxProgress);
        progressDialog.show();
    }

    private void createProgressBarForMergePDF() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.msg_progress_merger_pdf));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

}