package com.xiaopo.flying.stickerview.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.xiaopo.flying.stickerview.R;
import com.xiaopo.flying.stickerview.model.MyListData;

import java.util.ArrayList;

public class QuestionSampleAdapter extends RecyclerView.Adapter<QuestionSampleAdapter.QuestionSampleViewholder> {

    private MyListData[] listdata;
    Context context;


    public QuestionSampleAdapter(Context context, MyListData[] listdata) {
        this.context =context;
        this.listdata =listdata;
    }

    @Override
    public QuestionSampleAdapter.QuestionSampleViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.question_sample,parent,false);
        return new QuestionSampleAdapter.QuestionSampleViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(QuestionSampleAdapter.QuestionSampleViewholder holder, int position) {
        final MyListData myListData = listdata[position];
        holder.imgView.setImageResource(myListData.getImgId());
    }


    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public class QuestionSampleViewholder extends RecyclerView.ViewHolder {

        ImageView imgView;
        RelativeLayout rlMain;

        public QuestionSampleViewholder(View itemView) {
            super(itemView);
            imgView = itemView.findViewById(R.id.imgView);
            rlMain = itemView.findViewById(R.id.rlMain);
        }
    }
}

