package com.xiaopo.flying.stickerview.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.xiaopo.flying.sticker.DrawableSticker;
import com.xiaopo.flying.sticker.StickerView;
import com.xiaopo.flying.stickerview.ImagesActivity;
import com.xiaopo.flying.stickerview.model.MyListData;
import com.xiaopo.flying.stickerview.R;

import java.util.ArrayList;
import java.util.List;

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHolder>{
    private MyListData[] listdata;
    Context context;

    private List<StickerView> ivProfilePicArr = new ArrayList<StickerView>();

    // RecyclerView recyclerView;
    public MyListAdapter(MyListData[] listdata, Context context) {
        this.listdata = listdata;
        this.context = context;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.custome_images, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final MyListData myListData = listdata[position];
        holder.ivProfilePic.setImageResource(myListData.getImgId());
        holder.ivProfilePic.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    int x1 = (int) event.getX();
                    int y1 = (int) event.getY();

//                    ImagesActivity t = (ImagesActivity)v.getContext();
//                    t.check();
                    //placeImage(x, y);
                    int globalImage = ImagesActivity.getGlobalImage();
                    if(globalImage == 0) {
                        return false;
                    }

                    Drawable drawable1 = ContextCompat.getDrawable(context, globalImage);
                    // stickerView.addSticker(new DrawableSticker(drawable));
                    StickerView s = check(position).addSticker(new DrawableSticker(drawable1), 0 | 0);
                    s.getCurrentSticker().getMatrix().setTranslate(x1, y1);
                    Log.d("position", "position" + position);
                    Log.d("x", "x"+  x1);
                    Log.d("y", "y"+  y1);

                }
                return false;
            }
        });
    }


    public StickerView check(int position) {
        StickerView s = ivProfilePicArr.get(position);
        return s;
    }

    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivProfilePic;

        //public RelativeLayout relativeLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            this.ivProfilePic = (ImageView) itemView.findViewById(R.id.ivProfilePic);
          //  this.textView = (TextView) itemView.findViewById(R.id.textView);
          //  relativeLayout = (RelativeLayout)itemView.findViewById(R.id.relativeLayout);
        }
    }
}
