package com.xiaopo.flying.stickerview.util;

public class QuestionResponse {
    public Integer getQuestion() {
        return question;
    }

    public QuestionResponse(Integer question) {
        this.question = question;
    }

    public void setQuestion(Integer question) {
        this.question = question;
    }

    private Integer question;
}
