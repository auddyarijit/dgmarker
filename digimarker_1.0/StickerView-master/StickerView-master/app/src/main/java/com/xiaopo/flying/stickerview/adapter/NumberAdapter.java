package com.xiaopo.flying.stickerview.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.xiaopo.flying.stickerview.ImagesActivity;
import com.xiaopo.flying.stickerview.R;

import java.util.ArrayList;

public class NumberAdapter  extends RecyclerView.Adapter<NumberAdapter.NumberViewholder> {

    Activity activity;
    ArrayList<Integer> marksList = new ArrayList<Integer>();



    public NumberAdapter(Activity activity, ArrayList<Integer> marksList) {
        this.activity =activity;
        this.marksList =marksList;
    }

    @Override
    public NumberAdapter.NumberViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custome_number,parent,false);
        return new NumberAdapter.NumberViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(NumberAdapter.NumberViewholder holder, int numPostion) {

        holder.txtQue.setText(String.valueOf(marksList.get(numPostion)));

        holder.rlMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ImagesActivity)activity).getNumberPosition(numPostion+1);
            }
        });
    }



    @Override
    public int getItemCount() {
        return marksList.size();
    }

    public class NumberViewholder extends RecyclerView.ViewHolder {

        TextView txtQue;
        RelativeLayout rlMain;

        public NumberViewholder(View itemView) {
            super(itemView);
            txtQue = itemView.findViewById(R.id.txtQue);
            rlMain = itemView.findViewById(R.id.rlMain);
        }
    }
}


