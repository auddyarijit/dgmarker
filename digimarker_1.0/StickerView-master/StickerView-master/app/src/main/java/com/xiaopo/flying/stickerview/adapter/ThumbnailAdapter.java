package com.xiaopo.flying.stickerview.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.xiaopo.flying.sticker.DrawableSticker;
import com.xiaopo.flying.sticker.StickerView;
import com.xiaopo.flying.sticker.TextSticker;
import com.xiaopo.flying.stickerview.BookletViewActivity;
import com.xiaopo.flying.stickerview.ImagesActivity;
import com.xiaopo.flying.stickerview.model.BookletResponse;
import com.xiaopo.flying.stickerview.model.MyListData;
import com.xiaopo.flying.stickerview.R;
import com.xiaopo.flying.stickerview.remote.PhotoResponse;
import com.xiaopo.flying.stickerview.util.uuuuuu.BubbleInputDialog;
import com.xiaopo.flying.stickerview.util.uuuuuu.BubbleTextView;
import com.xiaopo.flying.stickerview.util.uuuuuu.DemoStickerView;

import java.util.ArrayList;
import java.util.List;

public class ThumbnailAdapter extends RecyclerView.Adapter<ThumbnailAdapter.InvoiceHistoryViewHolder> {

    private Activity activity;

    private Context context;
    // private MyListData[] listdata;
    private List<BookletResponse> bookletResponseList = new ArrayList<>();
    public static List<StickerView> ivProfilePicArr = new ArrayList<StickerView>();
    String url;

    int selectedPos = 0;
    public static int xCord = 0;
    public static int yCord = 0;
    public static int globalPosition = 0;


    Bitmap remarkBitmap;
    public StickerView stickerView;

    String circleId, circlePosition;


    View updateview;
    private BubbleInputDialog mBubbleInputDialog;
    private DemoStickerView mCurrentView;
    private BubbleTextView mCurrentEditTextView;
    private ArrayList<View> mViews = new ArrayList<>();
    private RelativeLayout mContentRootView;
    private View mAddBubble;


    public class InvoiceHistoryViewHolder extends RecyclerView.ViewHolder {

        // public TextView tvTitle;
        public ImageView ivProfilePic;
        public FrameLayout frame;
        public StickerView sticker_view;
        public RelativeLayout rl_content_root;

        public InvoiceHistoryViewHolder(View itemView) {
            super(itemView);
            // tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            ivProfilePic = (ImageView) itemView.findViewById(R.id.ivProfilePic);
            frame = itemView.findViewById(R.id.frame);
            sticker_view = itemView.findViewById(R.id.sticker_view);
            rl_content_root = itemView.findViewById(R.id.rl_content_root);

        }
    }


    public ThumbnailAdapter(Context context,/*MyListData[] listdata*/ List<BookletResponse> bookletResponseList) {
        this.context = context;
        this.bookletResponseList = bookletResponseList;

        // this.listdata = listdata;
    }


    /**
     * This is set from PDFCreateByXML class
     * This is my own model. This model have to set data from api
     *
     * @param bookletResponseList
     */
    public void setListData(List<BookletResponse> bookletResponseList) {
        this.bookletResponseList = bookletResponseList;
        notifyDataSetChanged();
    }

    @Override
    public InvoiceHistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custome_images, parent, false);
        return new InvoiceHistoryViewHolder(itemView);
    }


    public static StickerView check(int position) {
        StickerView s = ivProfilePicArr.get(position);
        return s;
    }


    @Override
    public void onBindViewHolder(InvoiceHistoryViewHolder holder, int position) {
        BookletResponse dataList = bookletResponseList.get(position);
        //  final MyListData myListData = listdata[position];
        holder.ivProfilePic.setImageResource(dataList.getImage());


        ((ImagesActivity) context).bitmapPostion(position);

        //  holder.ivProfilePic.setImageResource(myListData.getImgId());

//        new ArrayList<>();
//        mBubbleInputDialog = new BubbleInputDialog(context);
//        mBubbleInputDialog.setCompleteCallBack(new BubbleInputDialog.CompleteCallBack() {
//            @Override
//            public void onComplete(View bubbleTextView, String str) {
//                ((BubbleTextView) bubbleTextView).setText(str);
//            }
//        });


      /*  url = dataList.getUrl();

        //holder.tvTitle.setText(title);

          Picasso.Builder builder = new Picasso.Builder(context);
            builder.downloader(new OkHttp3Downloader(context));
            builder.build().load(url)
                    .into(holder.ivProfilePic);*/
        ivProfilePicArr.add(holder.sticker_view);


        holder.ivProfilePic.setOnTouchListener(new View.OnTouchListener()
        {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    int x1 = (int) event.getX();
                    int y1 = (int) event.getY();

//                    ImagesActivity t = (ImagesActivity)v.getContext();
//                    t.check();
                    //placeImage(x, y);
                    int globalImage = ImagesActivity.getGlobalImage();


//                    if (globalImage==R.drawable.remark_text)
//                    {
//                        remarkAlert();
//                    }


                    ArrayList<Integer> id = ImagesActivity.getValue();
                    //   Toast.makeText(t, "" + id, Toast.LENGTH_SHORT).show();

                    ((ImagesActivity) context).setAdapterNumber();


                    //  ((ImagesActivity)context).addBubble(holder);

//                    if(context instanceof ImagesActivity){
//                        ImagesActivity activity = (ImagesActivity)context;
//                        activity.addBubble(position);
//                    }

                    // addBubble(holder);


                    Log.d("x1", "x1 + y1 " + x1 + y1);
                    xCord = x1;
                    yCord = y1;
                    globalPosition = position;

                    ((ImagesActivity) context).numberPosition(holder, globalPosition);

                    ((ImagesActivity) context).getCordinatesX(x1);
                    ((ImagesActivity) context).getCordinatesY(y1);

                    int cordinatesX = ((ImagesActivity) context).getCordinatesX(x1);
                    int cordinatesY = ((ImagesActivity) context).getCordinatesY(y1);

                    if (globalImage == 0) {
                        return false;
                    } else {

                    }

                    Drawable drawable1 = ContextCompat.getDrawable(context, globalImage);
                    // stickerView.addSticker(new DrawableSticker(drawable));
                    StickerView s = check(position).addSticker(new DrawableSticker(drawable1), 0 | 0);
                    s.getCurrentSticker().getMatrix().setTranslate(x1, y1);


                    Log.d("Tag", "sttttt" + s.getCurrentSticker());
                    Log.d("position", "position" + position);
                    Log.d("x", "x" + x1);
                    Log.d("y", "y" + y1);

                }
                return false;
            }
        });
    }


    @Override
    public int getItemCount() {
        return bookletResponseList.size();
        //return listdata.length;
    }


    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, List<PhotoResponse> photoResponse) {
        photoResponse.add(position, (PhotoResponse) photoResponse);
        notifyItemInserted(position);
    }


    public void drawRemarkkk(String text, int textSize) {
        TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
        textPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        textPaint.setColor(Color.RED);
        textPaint.setTextSize(textSize);

        StaticLayout mTextLayout = new StaticLayout(text, textPaint,
                200, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

        // Create bitmap and canvas to draw to
        remarkBitmap = Bitmap.createBitmap(200, 40, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(remarkBitmap);

        // Draw background
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG
                | Paint.LINEAR_TEXT_FLAG);

        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setColor(Color.WHITE);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(30);
        c.drawPaint(paint);

        // Draw text
        c.save();
        c.translate(0, 0);
        mTextLayout.draw(c);
        c.restore();


        Drawable mDrawable = new BitmapDrawable(remarkBitmap);
        ivProfilePicArr.add(stickerView);
        StickerView sss = check(globalPosition).addSticker(new DrawableSticker(mDrawable), 0 | 0);
        sss.getCurrentSticker().getMatrix().setTranslate(xCord, yCord);
        Log.d("TAG", "drawText: " + remarkBitmap);
    }


}

