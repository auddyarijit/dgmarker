package com.xiaopo.flying.stickerview.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.xiaopo.flying.stickerview.BookletViewActivity;
import com.xiaopo.flying.stickerview.R;

import java.util.ArrayList;
import java.util.List;

public class ViewImageAdapter  extends RecyclerView.Adapter<ViewImageAdapter.ViewImageholder> {

    Activity activity;
    List<Bitmap> bitmapArrayList = new ArrayList<Bitmap>();



    public ViewImageAdapter(Activity activity, List<Bitmap> bitmapArrayList) {
        this.activity =activity;
        this.bitmapArrayList =bitmapArrayList;
        notifyDataSetChanged();
    }

    @Override
    public ViewImageAdapter.ViewImageholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.edited_image_view,parent,false);
        return new ViewImageAdapter.ViewImageholder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewImageAdapter.ViewImageholder holder, int position) {
        holder.imgView.setImageBitmap(bitmapArrayList.get(position));
    }



    @Override
    public int getItemCount() {
        return bitmapArrayList.size();
    }

    public void setListData(List<Bitmap> bitmapArrayList) {
        this.bitmapArrayList =bitmapArrayList;
        notifyDataSetChanged();
    }

    public class ViewImageholder extends RecyclerView.ViewHolder {

        ImageView  imgView;
        RelativeLayout rlMain;

        public ViewImageholder(View itemView) {
            super(itemView);
            imgView = itemView.findViewById(R.id.imgView);
            rlMain = itemView.findViewById(R.id.rlMain);
        }
    }
}
