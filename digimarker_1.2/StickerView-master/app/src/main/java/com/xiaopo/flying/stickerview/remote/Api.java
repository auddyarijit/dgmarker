package com.xiaopo.flying.stickerview.remote;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api {


    @GET("photos")
    Call<List<PhotoResponse>> photoResponse(@Query("albumId") int albumId);

//    @GET("posts")
//    Call<List<PostSResponse>> postsResponse(@Query("userId") int userId);

}
