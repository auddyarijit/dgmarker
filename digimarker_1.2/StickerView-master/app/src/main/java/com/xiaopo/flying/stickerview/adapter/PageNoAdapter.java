package com.xiaopo.flying.stickerview.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.xiaopo.flying.stickerview.ImagesActivity;
import com.xiaopo.flying.stickerview.R;

import java.util.ArrayList;

public class PageNoAdapter extends RecyclerView.Adapter<PageNoAdapter.PageNoViewholder> {

    Activity activity;
    ArrayList<Integer> pageNoList = new ArrayList<Integer>();



    public PageNoAdapter(Activity activity, ArrayList<Integer> pageNoList) {
        this.activity =activity;
        this.pageNoList =pageNoList;
    }

    @Override
    public PageNoAdapter.PageNoViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custome_number,parent,false);
        return new PageNoAdapter.PageNoViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(PageNoAdapter.PageNoViewholder holder, int pageNumPostion) {

        holder.txtQue.setText(String.valueOf(pageNoList.get(pageNumPostion)));

        holder.rlMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ImagesActivity)activity).getPageNoPosition(holder,pageNumPostion);
            }
        });
    }



    @Override
    public int getItemCount() {
        return pageNoList.size();
    }

    public class PageNoViewholder extends RecyclerView.ViewHolder {

        TextView txtQue;
        RelativeLayout rlMain;

        public PageNoViewholder(View itemView) {
            super(itemView);
            txtQue = itemView.findViewById(R.id.txtQue);
            rlMain = itemView.findViewById(R.id.rlMain);
        }
    }
}


