package com.xiaopo.flying.stickerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.xiaopo.flying.stickerview.adapter.QuestionSampleAdapter;
import com.xiaopo.flying.stickerview.model.MyListData;

public class SampleQuestionActivity extends AppCompatActivity {


    private RecyclerView recyclerView;
    QuestionSampleAdapter questionSampleAdapter;


    MyListData[] queSampleList = new MyListData[] {
            new MyListData( R.drawable.ma),
            new MyListData( R.drawable.ma),
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_question);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = findViewById(R.id.recyclerView);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        setAdapterSamplePaper();
    }


    private void setAdapterSamplePaper() {
        questionSampleAdapter = new QuestionSampleAdapter(this,queSampleList);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(questionSampleAdapter);
    }

}