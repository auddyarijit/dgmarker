package com.xiaopo.flying.stickerview.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.xiaopo.flying.stickerview.ImagesActivity;
import com.xiaopo.flying.stickerview.R;

import java.util.ArrayList;

public class QuestionMarksAdapter extends RecyclerView.Adapter<QuestionMarksAdapter.QuestionMarksViewholder> {

    Activity activity;
    ArrayList<Integer> queList = new ArrayList<Integer>();



    int selecteditem =0;


    public QuestionMarksAdapter(Activity activity, ArrayList<Integer> queList) {
        this.activity =activity;
        this.queList =queList;
    }

    @Override
    public QuestionMarksAdapter.QuestionMarksViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView =LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custome_question,parent,false);
        return new QuestionMarksAdapter.QuestionMarksViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(QuestionMarksAdapter.QuestionMarksViewholder holder, int position) {

        holder.txtQue.setText(String.valueOf("Q"+queList.get(position)));

        holder.rlMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 ((ImagesActivity)activity).getQuestionNumberPosition(position +1);
            }
        });

    }



    @Override
    public int getItemCount() {
        return queList.size();
    }

    public class QuestionMarksViewholder extends RecyclerView.ViewHolder {

        TextView txtQue;
        RelativeLayout rlMain;

        public QuestionMarksViewholder(View itemView) {
            super(itemView);
            txtQue = itemView.findViewById(R.id.txtQue);
            rlMain = itemView.findViewById(R.id.rlMain);
        }
    }
}
