package com.xiaopo.flying.stickerview.pdf;



import android.util.Log;

import com.xiaopo.flying.stickerview.R;
import com.xiaopo.flying.stickerview.model.BookletResponse;
import com.xiaopo.flying.stickerview.pdf.utils.PDFCreationUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PDFModel {

    private boolean isPending;
    private boolean isReceived;
    private String price;
    private String name;
    private float rating;
    private int image;

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public boolean isPending() {
        return isPending;
    }

    public void setPending(boolean pending) {
        isPending = pending;
    }

    public boolean isReceived() {
        return isReceived;
    }

    public void setReceived(boolean received) {
        isReceived = received;
    }




    public void setName(String name) {
        this.name = name;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * Create dummy PDF model
     *
     * @return PDF Models
     */

    public static List<PDFModel> createDummyPdfModel() {
        PDFCreationUtils.filePath.clear();
        PDFCreationUtils.progressCount = 1;

        boolean isFirstReceivedItem = false;
        List<PDFModel> pdfModels = new ArrayList<>();

        PDFModel bookletResponse = new PDFModel();
        bookletResponse.setImage(R.drawable.image1);
        pdfModels.add(bookletResponse);

        bookletResponse = new PDFModel();
        bookletResponse.setImage(R.drawable.image2);
        pdfModels.add(bookletResponse);

        bookletResponse = new PDFModel();
        bookletResponse.setImage(R.drawable.image3);
        pdfModels.add(bookletResponse);


        for (int i = 0; i <30; i++) {


           // Log.d("TAG", "createDummyPdfModel: " + pdfModels.get(i));

            Random rand = new Random();
            int price = rand.nextInt((1000 - 200) + 1) + 200;

            PDFModel model = new PDFModel();
            model.setImage(i);
            if (!isFirstReceivedItem) {
                model.setReceived(true);
                isFirstReceivedItem = true;
            } else {
                model.setReceived(false);
            }

            model.setPrice(String.valueOf(price) + String.valueOf(".0 Rs."));

            if (i % 4 == 0) {
                model.setName("Umesh Kumar " + i);
            } else {
                model.setName("Ram Kumar " + i);
            }
            model.setRating(i % 3);
            pdfModels.add(model);
        }

        return pdfModels;
    }
}