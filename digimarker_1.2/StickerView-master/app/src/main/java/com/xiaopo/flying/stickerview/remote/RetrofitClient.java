package com.xiaopo.flying.stickerview.remote;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private  static String BASE_URL ="https://jsonplaceholder.typicode.com/";
    private  static String BASE_URL_LOGIN ="https://reqres.in/api/";
    private static RetrofitClient retrofitClient;
    private static Retrofit retrofit;

    private RetrofitClient()
    {
        /*Gson gson = new GsonBuilder()
                .setLenient()
                .create();*/



        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    public static synchronized RetrofitClient getInstance()
    {
        if (retrofitClient ==null)
        {
            retrofitClient = new RetrofitClient();
        }
        return retrofitClient;
    }


    public Api getApi()
    {
        return retrofit.create(Api.class);
    }
}
