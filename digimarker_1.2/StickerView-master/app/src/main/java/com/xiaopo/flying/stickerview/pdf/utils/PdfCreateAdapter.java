package com.xiaopo.flying.stickerview.pdf.utils;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.xiaopo.flying.stickerview.R;
import com.xiaopo.flying.stickerview.model.BookletResponse;
import com.xiaopo.flying.stickerview.pdf.PDFModel;

import java.util.List;

public class PdfCreateAdapter extends RecyclerView.Adapter<PdfCreateAdapter.MyViewHolder> {

    private List<BookletResponse> pdfModels;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pdf_creation, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BookletResponse model = pdfModels.get(position);
        if (model != null) {
            if (model.isReceived()) {
                holder.mReceivedTV.setVisibility(View.VISIBLE);
            } else {
                holder.mReceivedTV.setVisibility(View.GONE);
            }

            holder.imgPaper.setImageResource(model.getImage());
            holder.mPriceTV.setText(model.getPrice());
        }
    }

    @Override
    public int getItemCount() {
        return pdfModels.size();
    }

    /**
     * This is set from PDFCreateByXML class
     * This is my own model. This model have to set data from api
     *
     * @param pdfModels
     */
    public void setListData(List<BookletResponse> pdfModels) {
        this.pdfModels = pdfModels;
        notifyDataSetChanged();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView mReceivedTV;
        private final TextView mNameTV;
        private final ImageView mRateIM;
        private final ImageView imgPaper;
        private final TextView mPriceTV;

        public MyViewHolder(View view) {
            super(view);
            mPriceTV = (TextView) view.findViewById(R.id.tv_price);
            mReceivedTV = (TextView) view.findViewById(R.id.tv_received);
            mNameTV = (TextView) view.findViewById(R.id.tv_name);
            mRateIM = (ImageView) view.findViewById(R.id.iv_rate);
            imgPaper = (ImageView) view.findViewById(R.id.imgPaper);
        }
    }

}