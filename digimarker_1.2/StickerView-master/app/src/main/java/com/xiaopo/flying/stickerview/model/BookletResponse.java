package com.xiaopo.flying.stickerview.model;

import android.util.Log;

import com.xiaopo.flying.stickerview.pdf.utils.PDFCreationUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BookletResponse {
    private int image;
    private boolean isPending;
    private boolean isReceived;
    private String price;
    private String name;

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }


    public boolean isPending() {
        return isPending;
    }

    public void setPending(boolean pending) {
        isPending = pending;
    }

    public boolean isReceived() {
        return isReceived;
    }

    public void setReceived(boolean received) {
        isReceived = received;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }



}
